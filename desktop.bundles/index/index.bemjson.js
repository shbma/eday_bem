module.exports = {
    block : 'page',
    title : 'БЭМ',
    favicon : '/favicon.ico',
    head : [
        { elem : 'meta', attrs : { name : 'description', content : '' } },
        { elem : 'meta', attrs : { name : 'charset', content : 'utf-8' } },
        { elem : 'meta', attrs : { name : 'viewport', content : 'width=device-width, initial-scale=1' } },
        { elem : 'css', url : 'index.min.css' }
    ],
    scripts: [
        { elem : 'js', url : 'index.min.js' },
        { elem : 'js', url : 'jquery-3.0.0.js' },
    ],
    //mods : { theme : 'islands' },
    content : [
        {
            block: 'joystick',
            content: [
                {
                    elem: 'step-up',
                    content: ''
                },
                {
                    elem: 'step-down',
                    content: ''
                },
            ]
        },
        {
            block: 'slide',   
            mods: {verticalAlign: 'center'},         
            content: [
                {
                    elem: 'butthead',
                    content:''
                },                                                
                {
                    elem: 'header',
                    elemMods: {order: 'first-slide'},                    
                    content:'БЭМ'
                },      
                {
                    elem: 'sub-header',
                    elemMods: {order: 'first-slide'},                    
                    content:'Методология создания сайтов<br>от Яндекса'
                }                                            
            ]
        },
        {
            block: 'slide',               
            content: [                                             
                {
                    elem: 'header',                                    
                    content:'Определение'
                }, 
                {
                    elem: 'body',                                    
                    content:[
                        {
                            block: 'text-item',
                            content: 'БЭМ - методология, созданная в Яндексе для разработки сайтов, которые надо делать быстро, а поддерживать долгие годы.​'
                        },
                        {
                            block: 'text-item',
                            content: 'БЭМ предоставляет комплексное решение по созданию архитектуры проекта и помогает организовать процессы разработки.'
                        },
                        {
                            block: 'text-item',
                            content: 'Компоненты БЭМ:'
                        },
                        {
                            block : 'tripod-image',
                            content: ''                        
                        }
                    ]
                }   
            ]
        },
        {
            block: 'slide',               
            content: [                                             
                {
                    elem: 'header',                                    
                    content:'Зачем?'
                }, 
                {
                    elem: 'body',                                    
                    content:[
                        {
                            block: 'text-item',
                            content: 'Сократить сроки и себестоимость разработки​'
                        },
                        {
                            block: 'text-item',
                            content: 'Сократить затраты на поддержку'
                        }
                    ]
                }   
            ]
        }, 
        {
            block: 'slide',               
            content: [                                             
                {
                    elem: 'header',                                    
                    content:'Каким образом?'
                }, 
                {
                    elem: 'body',                                    
                    content:[
                        {
                            block: 'text-item',                            
                            content: [
                                {
                                    block: 'text-brick',
                                    content: 'Повышение Индивидуальной эффективности'
                                },
                                {
                                    block: 'text-item',
                                    mods:{shift:'left1'},
                                    content: [
                                        {
                                            block: 'text-brick',
                                            content: 'Повторное использование кода'
                                        },
                                        [
                                            {
                                                block: 'text-item',
                                                mods: {
                                                    shift: 'left1'
                                                },
                                                content: {
                                                    block: 'text-brick',
                                                    content: 'Инкапсуляция реализаций блоков'
                                                }
                                            },
                                            {
                                                block: 'text-item',
                                                mods: {
                                                    shift: 'left1'
                                                },
                                                content: {
                                                    block: 'text-brick',
                                                    content: 'Пространства имен (свое для каждого блока)'
                                                }
                                            },
                                            {
                                                block: 'text-item',
                                                mods: {
                                                    shift: 'left1'
                                                },
                                                content: {
                                                    block: 'text-brick',
                                                    content: 'Наследование блоков, yровни переопределения, примеси'
                                                }
                                            }
                                        ]
                                    ]
                                },
                                [
                                    {
                                        block: 'text-item',
                                        mods: {
                                            shift: 'left1'
                                        },
                                        content: {
                                            block: 'text-brick',
                                            content: 'Улучшенная человекопонятность, самодокументируемость'
                                        }
                                    },
                                    {
                                        block: 'text-item',
                                        mods: {
                                            shift: 'left1'
                                        },
                                        content: {
                                            block: 'text-brick',
                                            content: 'Простота обновления и масштабирования'
                                        }
                                    },
                                    {
                                        block: 'text-item',
                                        mods: {
                                            shift: 'left1'
                                        },
                                        content: {
                                            block: 'text-brick',
                                            content: 'Автоматизация сборки кода'
                                        }
                                    }
                                ]

                            ]
                        },
                        {
                            block: 'text-item',
                            mods: {'extra-space':'top'},
                            content: [
                                {
                                    block: 'text-brick',
                                    content: 'Повышение Командной эффективности'
                                },
                                {
                                    block: 'text-item',
                                    mods: {
                                        shift: 'left1'
                                    },
                                    content: {
                                        block: 'text-brick',
                                        content: "Единая предметная область, общая терминология"
                                    }
                                },
                                {
                                    block: 'text-item',
                                    mods: {
                                        shift: 'left1'
                                    },
                                    content: {
                                        block: 'text-brick',
                                        content: "Ускорение ввода нового человека в проект"
                                    }
                                }
                            ]
                        }                        
                    ]
                }   
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Основа'
                },
                {
                    block: 'body',
                    content: [
                        {
                            block: 'text-item',
                            content: 'Соглашение по <b>Именованию</b>'
                        },
                        {
                            block: 'text-item',
                            content: "Соглашение по <b>Организации файловой структуры</b>"
                        },
                        {
                            block: 'text-item',
                            content: "Инструменты <b>Сборки</b> (*  .js)"
                        }
                    ]
                }
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Соглашение по именованию'
                },
                {
                    block: 'body',
                    content: 
                    [
                        {
                            block: 'text-item',
                            content: 'БЭМ = Блок__Элемент_Модификатор_[значение-модификатора]'
                        },
                        {
                            block: 'cat',
                            content: {}
                        }
                    ]
                }
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Соглашение по именованию'
                },
                {
                    elem: 'body',
                    content: [
                        {
                            block: 'text-item',
                            content: 'Какие БЭМ сущности можно выделить на изображении ниже?'
                        },
                        {
                            block: 'hat',
                            content: {}
                        }
                    ]
                }
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Файловая структура'
                },
                {
                    block: 'body',
                    content: [
                        {
                            block: 'text-item',
                            content: 'Файлы объединяются по смыслу, а не по типу'
                        },
                        {
                            block: 'text-item',
                            content: 'Блоку соответствует директория в файловой системе. Имена блока и его директории совпадают.'
                        },
                        {
                            block: 'text-item',
                            content: 'Опциональные элементы и модификаторы выносятся в отдельные файлы'
                        },
                        {
                            block: 'text-item',
                            content: "Проект разделяется на уровни переопределения"
                        },
                        {
                            block: 'text-item',
                            mods: {'extra-space':'top'},
                            content: [
                                {
                                    block: 'text-brick',
                                    content: 'Например:'
                                },
                                {
                                    block: 'text-brick',
                                    content: 'blocks/<br>&nbsp;&nbsp;&nbsp;input/<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input.css<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input.js<br>&nbsp;&nbsp;&nbsp;button/<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;button.css<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;button.js<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;button.png<br>'
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Процедура создания проекта'
                },
                {
                    block: 'body',
                    content: [
                        {
                            block: 'text-item',
                            content:{
                                    block: 'link',
                                    mods : { theme : 'islands', size : 'xl' },
                                    url : 'https://ru.bem.info/platform/tutorials/quick-start-static/',
                                    content : 'Создание статической страницы',
                                    target : '_blank'
                                }      
                            
                        },
                        {
                            block: 'text-item',
                            content: {
                                block: 'link',
                                mods : { theme : 'islands', size : 'xl' },
                                url : 'https://ru.bem.info/platform/tutorials/start-with-project-stub/',
                                content : 'Проект со страницами каталога и контактов',
                                target : '_blank'
                            }
                        }
                    ]
                    
                }
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Архитектура системы (БЭМ + backend)'
                },
                {
                    block: 'body',
                    content: [
                        {
                            block: 'text-item',
                            content: 'Отказаться от PHP в пользу node.js'
                        },
                        {
                            block: 'text-item',
                            content: 'Использовать портированный на PHP сборщик'
                        },
                        {
                            block: 'text-item',
                            content: 'Фронтенд на node.js перед PHP.<br> Схема:&nbsp;&nbsp;БД&nbsp;->&nbsp;PHP&nbsp;->&nbsp;node.js&nbsp;->&nbsp;браузер.'
                        },
                        {
                            block: 'text-item',
                            content: 'Использовать расширение PHP v8js. Как п.1, только JS-код выполняется внутри PHP.<br> Медленно, т.к. на каждый запрос будет заново создаваться JS runtime.'
                        },
                        {
                            block: 'text-item',
                            content: 'Заранее подготовить весь контекст и использовать только вывод нужных ключей контекста в определенных местах страницы, то можно делать вставки PHP-кода прямо в bemjson.js и BEMHTML-шаблонах. А бандл собирать с расширением php вместо html и его инклюдить из приложения в качестве темы.'
                        },
                        {
                            block: 'text-item',
                            content: 'Использовать БЭМ-стек только на этапе подготовки статики, а потом резать полученный HTML на традиционные PHP-шаблоны. <br>Но в этом случае дальнейшие изменения в разметку придется вносить уже в эти PHP-шаблоны, а БЭМ-инфраструктуру использовать только для написания и пересборки CSS и JS.'
                        },
                        
                    ]
                },                
            ]
        },
        {
            block: 'slide',
            content: [
                {
                    elem: 'header',
                    content: 'Источинки'
                },
                {
                    block: 'body',
                    content: 
                        [
                            {
                                block: 'text-item-sub',
                                content:  'https://ru.bem.info/ - Сайт проекта'
                            },
                            {
                                block: 'text-item-sub',
                                content: 'https://www.youtube.com/watch?v=VD_l1feBwTk - В.Гриненко "Автоматизация БЭМ" на Talks&Works2015 (видео)'
                            },
                            {
                                block: 'text-item-sub',
                                content: 'https://ru.bem.info/forum/174/ - Форум: по BEMHTML, applyNext и пр.',
                            },
                            {
                                block: 'text-item-sub',
                                content: 'http://tadatuta.github.io/bemmet/ - Плагин bemmet для Sublime <Text></Text>'
                            },
                            {
                                block: 'text-item-sub',
                                content: 'https://ru.bem.info/forum/45/ - Форум: БЭМ и php'
                            },
                            {
                                block: 'text-item-sub',
                                content: 'https://ru.bem.info/forum/175/ - php порт сборщика'
                            },
                        ]
                }
            ]
        }


    ]
};
