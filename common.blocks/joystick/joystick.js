//прокрутка
var joystick__currentSlide = 0;
var joystick__numberOfSlides = document.querySelectorAll('.slide').length;

document.querySelector(".joystick__step-down").addEventListener('click',function(){			
	doScroll('down')
})

document.querySelector(".joystick__step-up").addEventListener('click',function(){			
	doScroll('up')
})

document.querySelector("body").addEventListener('keydown',function(event){			
	var charCode = getChar(event);	
	switch (charCode){
		case 38: 
			event.preventDefault()			
			doScroll('up');	
			break;
		case 40:
			event.preventDefault()
			doScroll('down');	
			break;
	}
	
})


/* Получает код нажатой клавиши. Кроссбраузерно. 
event.type должен быть keypress */
function getChar(event) {
  if (event.which == null) { // IE
    if (event.keyCode < 32) return null; // спец. символ
    return event.keyCode
  }

  if (event.which != 0) { // все кроме IE
    if (event.which < 32) return null; // спец. символ
    return event.which; // остальные
  }

  return null; // спец. символ
}

/*Прыгает к следующему слайду. 
Направление листание direction - строка 'up' или 'down'*/
function doScroll(direction){
	if (direction === 'up'){
		joystick__currentSlide = (joystick__currentSlide > 0) 
				? joystick__currentSlide-1 
				: joystick__currentSlide	
	} else if (direction === 'down'){
		joystick__currentSlide = (joystick__currentSlide < joystick__numberOfSlides-1) 
				? joystick__currentSlide += 1 
				: joystick__currentSlide
	}	
	
	var vh = window.innerHeight/100;
	window.scrollTo(0,document.querySelectorAll('.slide')[joystick__currentSlide].offsetTop-vh);
}

// document.querySelector(".joystick__step-up").addEventListener('click',function(){			
// 	scroll_h -= scroll_frame;
// 	var scroll_handler = setInterval(function(){			
// 		if (window.pageYOffset < scroll_h){
// 			window.scrollBy(0,-20);			
// 		} else {
// 			clearInterval(scroll_handler);				
// 		}		
// 	},6)
// })


//window.onscroll = function(event) {
// 	//event.preventDefault()
// 	var scrolled = window.pageYOffset || document.documentElement.scrollTop;
// 	if (scrolled < joystick__oldScroll){
// 		//doScroll('up');
// 	} else {
// 		//doScroll('down');
// 	}	
// }